import { BeeJeePage } from './app.po';

describe('bee-jee App', function() {
  let page: BeeJeePage;

  beforeEach(() => {
    page = new BeeJeePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('BeeJee works!');
  });
});
