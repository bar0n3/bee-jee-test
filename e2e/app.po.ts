import { browser, element, by } from 'protractor';

export class BeeJeePage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('BeeJee-root h1')).getText();
  }
}
