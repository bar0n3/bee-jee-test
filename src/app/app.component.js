"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var tasks_service_1 = require("./services/tasks.service");
var Rx_1 = require("rxjs/Rx");
var AppComponent = (function () {
    function AppComponent(tasksService) {
        var _this = this;
        this.tasksService = tasksService;
        this.modelTask = {};
        this.modelTaskOnEdit = {};
        this.modelLogin = {};
        this.pagination = {};
        this.active = 'list';
        this.column = 'Id';
        this.isDesc = 0;
        this.tasksService.getTasks().subscribe(function (json) {
            _this.tasks = json.message.tasks;
            _this.totalTasks = json.message.total_task_count;
            var total = Math.ceil(_this.totalTasks / 3);
            _this.pagination = {
                total: total,
                current: 1,
                pages: Array(total).fill(1).map(function (x, i) { return i + 1; })
            };
        });
    }
    AppComponent.prototype.addTask = function () {
        var _this = this;
        this.tasks.push(this.modelTask);
        this.tasksService.createTask(this.modelTask, this.files).subscribe(function (data) {
            _this.tasksService.getTasks();
            return true;
        }, function (error) {
            console.error("Error on creating task!");
            throw Rx_1.Observable.throw(error);
        });
        this.modelTask = {};
    };
    AppComponent.prototype.editTask = function (k) {
        this.modelTaskOnEdit = this.tasks[k];
        this.onEditIndex = k;
    };
    AppComponent.prototype.updateTask = function () {
        var _this = this;
        for (var i = 0; i < this.tasks.length; i++) {
            if (i === this.onEditIndex) {
                this.tasks[i] = this.modelTaskOnEdit;
                this.tasksService.updateTask(this.tasks[i]).subscribe(function (data) {
                    _this.tasksService.getTasks();
                    return true;
                }, function (error) {
                    console.error("Error saving task!");
                    throw Rx_1.Observable.throw(error);
                });
                this.modelTaskOnEdit = {};
            }
        }
    };
    AppComponent.prototype.addPhoto = function (event) {
        var target = event.target || event.srcElement;
        this.files = target.files;
    };
    AppComponent.prototype.sortBy = function (column) {
        var _this = this;
        this.isDesc = this.column === column ? Number(!this.isDesc) : this.isDesc;
        this.column = column;
        var params = {
            sort_direction: !this.isDesc ? 'asc' : 'desc',
            sort_field: column.toLowerCase(),
            page: 1
        };
        this.pagination.current = 1;
        this.tasksService.getTasksByParams(params).subscribe(function (json) {
            _this.tasks = json.message.tasks;
            _this.totalTasks = json.message.total_task_count;
        });
    };
    AppComponent.prototype.canBePreviewed = function () {
        return (this.modelTask.email && this.modelTask.email.length > 0)
            && (this.modelTask.username && this.modelTask.username.length > 0)
            && (this.modelTask.text && this.modelTask.text.length > 0)
            && (this.files && this.files.length > 0);
    };
    AppComponent.prototype.login = function () {
        if ((this.modelLogin.username && this.modelLogin.username === 'Admin')
            && (this.modelLogin.password && this.modelLogin.password === '123')) {
            console.log(this.modelLogin.password, this.modelLogin.username);
            this.loginSuccess = 1;
        }
    };
    AppComponent.prototype.changeStatus = function (i) {
        if (this.loginSuccess > 0) {
            this.tasks[i].status = this.tasks[i].status < 10 ? 10 : 0;
            this.editTask(i);
            this.updateTask();
        }
    };
    AppComponent.prototype.goToPage = function (page) {
        var _this = this;
        this.pagination.current = page;
        var params = {
            sort_direction: !this.isDesc ? 'asc' : 'desc',
            sort_field: this.column.toLowerCase(),
            page: page
        };
        this.tasksService.getTasksByParams(params).subscribe(function (json) {
            _this.tasks = json.message.tasks;
            _this.totalTasks = json.message.total_task_count;
        });
    };
    AppComponent.prototype.goToNextPage = function () {
        if (this.pagination.current != this.pagination.total) {
            this.goToPage(this.pagination.current + 1);
        }
    };
    AppComponent.prototype.goToPrevPage = function () {
        if (this.pagination.current != 1) {
            this.goToPage(this.pagination.current - 1);
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css'],
        providers: [tasks_service_1.TasksService]
    }),
    __metadata("design:paramtypes", [tasks_service_1.TasksService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map