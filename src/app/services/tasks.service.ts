import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams} from "@angular/http";
import 'rxjs/add/operator/map';
import { Md5 } from '../md5';

@Injectable()

export class TasksService {
  constructor(private http: Http) {
    console.log('init task service');
  }
  host:string = 'https://uxcandy.com/~shapoval/test-task-backend/';

  getTasks() {
    return this.http.get(`${this.host}?developer=AlexeyKobzev`)
      .map(res => res.json())
  }

  getTasksByParams(paramsObj:any) {
    const params = Object.keys(paramsObj).map((key) => {
      return key + '=' + encodeURIComponent(paramsObj[key]);
    }).join('&');

    return this.http.get(`${this.host}?developer=AlexeyKobzev&${params}`)
      .map(res => res.json())
  }

  createTask(task:any, files :FileList) {
    const formData = new FormData();
    if (files) {
      formData.append('image', files[0]);
      formData.append('username', task.username);
      formData.append('email', task.email);
      formData.append('text', task.text);
    }

    return this.http.post(`${this.host}create/?developer=AlexeyKobzev`, formData )
      .map((res: Response) => res.json());
  }

  updateTask(task:any) {
    const formData = new FormData();
    formData.append('username', task.username);
    formData.append('email', task.email);
    formData.append('text', task.text);
    formData.append('status', task.status);
    formData.append('token', 'beejee');

    const data = [
      { email: task.email },
      { status: task.status },
      { text: task.text },
      { username: task.username },
      { token: 'beejee'}
    ];

    const dataEncoded = data.map((paramsObj) => {
      return Object.keys(paramsObj).map((key) => {
        return key + '=' + encodeURIComponent(paramsObj[key]);
      }).join('');
    }).join('&');

    const signature = Md5.hashStr(dataEncoded);
    formData.append('signature', signature);

    return this.http.post(`${this.host}edit/${task.id}?developer=AlexeyKobzev`, formData )
      .map((res: Response) => res.json());
  }
}
