"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var md5_1 = require("../md5");
var TasksService = (function () {
    function TasksService(http) {
        this.http = http;
        this.host = 'https://uxcandy.com/~shapoval/test-task-backend/';
        console.log('init task service');
    }
    TasksService.prototype.getTasks = function () {
        return this.http.get(this.host + "?developer=AlexeyKobzev")
            .map(function (res) { return res.json(); });
    };
    TasksService.prototype.getTasksByParams = function (paramsObj) {
        var params = Object.keys(paramsObj).map(function (key) {
            return key + '=' + encodeURIComponent(paramsObj[key]);
        }).join('&');
        return this.http.get(this.host + "?developer=AlexeyKobzev&" + params)
            .map(function (res) { return res.json(); });
    };
    TasksService.prototype.createTask = function (task, files) {
        var formData = new FormData();
        if (files) {
            formData.append('image', files[0]);
            formData.append('username', task.username);
            formData.append('email', task.email);
            formData.append('text', task.text);
        }
        return this.http.post(this.host + "create/?developer=AlexeyKobzev", formData)
            .map(function (res) { return res.json(); });
    };
    TasksService.prototype.updateTask = function (task) {
        var formData = new FormData();
        formData.append('username', task.username);
        formData.append('email', task.email);
        formData.append('text', task.text);
        formData.append('status', task.status);
        formData.append('token', 'beejee');
        var data = [
            { email: task.email },
            { status: task.status },
            { text: task.text },
            { username: task.username },
            { token: 'beejee' }
        ];
        var dataEncoded = data.map(function (paramsObj) {
            return Object.keys(paramsObj).map(function (key) {
                return key + '=' + encodeURIComponent(paramsObj[key]);
            }).join('');
        }).join('&');
        var signature = md5_1.Md5.hashStr(dataEncoded);
        formData.append('signature', signature);
        return this.http.post(this.host + "edit/" + task.id + "?developer=AlexeyKobzev", formData)
            .map(function (res) { return res.json(); });
    };
    return TasksService;
}());
TasksService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], TasksService);
exports.TasksService = TasksService;
//# sourceMappingURL=tasks.service.js.map