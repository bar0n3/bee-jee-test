import { Component } from '@angular/core';
import { TasksService } from "./services/tasks.service";
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TasksService]
})

export class AppComponent  {
  modelTask:any = {};
  modelTaskOnEdit:any = {};
  modelLogin:any = {};
  pagination:any = {};
  onEditIndex:any;
  previewSrc:string;
  active:string = 'list';
  column:string = 'Id';
  isDesc:number = 0;
  totalTasks:number;
  loginSuccess:number;
  files:any;
  tasks: task[];

  constructor(private tasksService: TasksService) {
    this.tasksService.getTasks().subscribe(json => {
      this.tasks = json.message.tasks;
      this.totalTasks = json.message.total_task_count;
      const total = Math.ceil(this.totalTasks / 3);
      this.pagination = {
        total: total,
        current: 1,
        pages: Array(total).fill(1).map((x:number, i:number) => i + 1)
      };
    })
  }

  addTask() {
    this.tasks.push(this.modelTask);

    this.tasksService.createTask(this.modelTask, this.files).subscribe(
      data => {
        this.tasksService.getTasks();
        return true;
      },
      error => {
        console.error("Error on creating task!");
        throw Observable.throw(error);
      }
    );
    this.modelTask = {};
  }

  editTask(k:any) {
    this.modelTaskOnEdit = this.tasks[k];
    this.onEditIndex = k;
  }

  updateTask() {
    for (let i=0; i<this.tasks.length; i++) {
      if (i === this.onEditIndex) {
        this.tasks[i] = this.modelTaskOnEdit;
        this.tasksService.updateTask(this.tasks[i]).subscribe(
          data => {
            this.tasksService.getTasks();
            return true;
          },
          error => {
            console.error("Error saving task!");
            throw Observable.throw(error);
          }
        );
        this.modelTaskOnEdit = {};
      }
    }
  }

  addPhoto(event:any) {
    let target = event.target || event.srcElement;
    this.files = target.files;
  }

  sortBy(column:string) {
    this.isDesc = this.column === column ? Number(!this.isDesc) : this.isDesc;
    this.column = column;

    const params = {
      sort_direction: !this.isDesc ? 'asc' : 'desc',
      sort_field: column.toLowerCase(),
      page: 1
    };

    this.pagination.current = 1;
    this.tasksService.getTasksByParams(params).subscribe(json => {
      this.tasks = json.message.tasks;
      this.totalTasks = json.message.total_task_count;
    });
  }

  canBePreviewed() {
    return (this.modelTask.email && this.modelTask.email.length > 0)
      && (this.modelTask.username && this.modelTask.username.length > 0)
      && (this.modelTask.text && this.modelTask.text.length > 0)
      && (this.files && this.files.length > 0);
  }

  login() {
    if ((this.modelLogin.username && this.modelLogin.username === 'Admin')
      && (this.modelLogin.password && this.modelLogin.password === '123')) {
        console.log(this.modelLogin.password,  this.modelLogin.username);
        this.loginSuccess = 1;
    }
  }

  changeStatus(i:number) {
    if (this.loginSuccess > 0) {
      this.tasks[i].status = this.tasks[i].status < 10 ? 10 : 0;
      this.editTask(i);
      this.updateTask();
    }
  }

  goToPage(page:number) {
    this.pagination.current = page;
    const params = {
      sort_direction: !this.isDesc ? 'asc' : 'desc',
      sort_field: this.column.toLowerCase(),
      page: page
    };
    this.tasksService.getTasksByParams(params).subscribe(json => {
      this.tasks = json.message.tasks;
      this.totalTasks = json.message.total_task_count;
    });
  }

  goToNextPage() {
    if (this.pagination.current != this.pagination.total) {
      this.goToPage(this.pagination.current + 1);
    }
  }

  goToPrevPage() {
    if (this.pagination.current != 1) {
      this.goToPage(this.pagination.current - 1);
    }
  }
}

interface task {
  id: number;
  username: string;
  email: string;
  text: string;
  status: number;
  image_path: string;
}


